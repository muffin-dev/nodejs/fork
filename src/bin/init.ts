import { Command } from 'commander';
import { InitOptions, IInitOptions, init } from '../lib';

export default function(program: Command) {
  program
    .command('init')
    .description('Initializes a new fork.')
    .argument('<original-url>', 'The URL to the original repository (should end with *.git).')
    .argument('<fork-url>', 'The URL to your own repository where the original one will be forked (should end with *.git).')
    .option('--originalBranch <name>', 'The name of the branch in the original repository from which you want your fork repository to start', 'main')
    .option('--forkBranch <name>', 'The name of the branch in your own repository where the original branch will be merged.', 'main')
    .option('--tmpBranch <name>', 'The name of the local branch that will handle the original branch fork before being merged', 'original/main')
    .option('--originalRemote <name>', 'The name of the remote to the original repository.', 'upstream')
    .option('--forkRemote <name>', 'The name of the remote to your own repository.', 'origin')
    .option('-p --push', 'If enabled, your new fork repository will be pushed automatically at the end of the operation.', false)
    .action(async (originalUrl: string, forkUrl: string, args: IInitOptions) => {
      args = new InitOptions(args);
      
      try {
        await init(originalUrl, forkUrl, args);
      }
      catch (error) {
        console.error(error);
      }
    });
}