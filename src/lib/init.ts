import { overrideObject } from '@muffin-dev/js-helpers';
import { execAsync } from './utilities';

/**
 * @class Represents the options for initializing a new fork repository.
 */
export class InitOptions {
  public originalBranch = 'main';
  public forkBranch = 'main';
  public tmpBranch = 'original/main';
  public originalRemote = 'upstream';
  public forkRemote = 'origin';
  public push = false;

  constructor(args: IInitOptions) {
    overrideObject(this, args);
  }
}

/**
 * @interface IInitOptions Represents the options for initializing a new fork repository.
 */
export interface IInitOptions extends Partial<InitOptions> { }

/**
 * Initialize a new fork repository.
 * @param originalUrl The URL of the original repository.
 * @param forkUrl The URL of your own repository where the original one will be forked.
 * @param options Options for initializing the fork repository.
 */
export async function init(originalUrl: string, forkUrl: string, options: IInitOptions) {
  // Ensure options default values
  options = new InitOptions(options);

  try {
    console.log('Initializing git repository...');
    await execAsync('git init');
    console.log('Adding remotes...');
    await execAsync(`git remote add ${options.forkRemote} ${forkUrl}`);
    await execAsync(`git remote add ${options.originalRemote} ${originalUrl}`);
    await execAsync(`git switch --orphan ${options.tmpBranch}`);
    console.log('Pulling upstream branch...');
    await execAsync(`git pull ${options.originalRemote} ${options.originalBranch}`);
    await execAsync(`git switch --orphan ${options.forkBranch}`);
    console.log('Merging branches...');
    await execAsync(`git merge ${options.tmpBranch}`);
  
    if (options.push) {
      console.log('Pushing the new fork branch...');
      await execAsync(`git push ${options.forkRemote} ${options.forkBranch}`);
    }

    console.log('Fork repository initialized successfully!');
  }
  catch (error) {
    console.error('Error while initializing new fork repository:', error);
  }
}